# 一、 根据MySQL数据库表在Hive数据仓库中构建相应的表：
## 1.1、用户信息表： tbl_users

````
sqoop create-hive-table \
--connect jdbc:mysql://chb1:3306/tags_dat \
--table tbl_users \
--username root \
--password 123456 \
--hive-table tags_dat.tbl_users \
--fields-terminated-by '\t' \
--lines-terminated-by '\n'
````

# 二、使用Sqoop将MySQL数据库表中的数据导入到Hive表中（本质就是存储在HDFS上）

使用Sqoop将MySQL数据库表中的数据导入到Hive表中（本质就是存储在HDFS上），具体命令如下：

1、用户信息表： tbl_users

```
sqoop import \
--connect jdbc:mysql://chb1:3306/tags_dat \
--username root \
--password 123456 \
--table tbl_users \
--direct \
--hive-overwrite \
--delete-target-dir \
--fields-terminated-by '\t' \
--lines-terminated-by '\n' \
--hive-table tags_dat.tbl_users \
--hive-import \
--num-mappers 1
```


```
sqoop create-hive-table \
--connect jdbc:mysql://chb1:3306/tags_dat \
--table tbl_users \
--username root \
--password 123456 \
--hive-table tags_dat.tbl_users \
--fields-terminated-by '\t' \
--lines-terminated-by '\n'
```




